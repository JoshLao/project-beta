import { useEffect, useState } from 'react';

function AppointmentForm() {
    const [technicians, setTechnicians] = useState([]);
    const [vin, setVin] = useState('');
    const [customer, setCustomer] = useState('');
    const [dateTime, setDateTime] = useState('');
    const [technicianId, setTechnicianId] = useState('');
    const [reason, setReason] = useState('');
    const [submittedApp, setSubmittedApp] = useState(false);

    const getData = async () => {
        const url = 'http://localhost:8080/api/technicians/'
        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();
            setTechnicians(data.technicians)
        }
    }
    useEffect(() => {
        getData();
    }, []);

    const handleSubmit = async (e) => {
        e.preventDefault();
        const appData = {};
        appData.vin = vin;
        appData.customer = customer;
        appData.date_time = dateTime;
        appData.technician_id = technicianId;
        appData.reason = reason;

        const appUrl = 'http://localhost:8080/api/appointments/'
        const fetchConfig = {
            method: "POST",
            body: JSON.stringify(appData),
            headers: {
                'Content-Type': 'application/json'
            },
        };

        const appResponse = await fetch(appUrl, fetchConfig);
        if (appResponse.ok) {
            setVin('');
            setCustomer('');
            setDateTime('')
            setTechnicianId('')
            setReason('');
        }
        setSubmittedApp(true);
    }

    const handleVinChange = (e) => {
        const value = e.target.value;
        setVin(value);
    }
    const handleCustomerChange = (e) => {
        const value = e.target.value;
        setCustomer(value);
    }
    const handleTimeChange = (e) => {
        const value = e.target.value;
        setDateTime(value);
    }
    const handleTechChange = (e) => {
        const value = e.target.value;
        setTechnicianId(value);
    }
    const handleReasonChange = (e) => {
        const value = e.target.value;
        setReason(value);
    }

    const formClasses = (!submittedApp) ? '' : 'd-none';
    const messageClasses = (!submittedApp) ? 'alert alert-success d-none mb-0' : 'alert alert-success mb-0';



    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Create a Service Appointment</h1>
                    <form className={formClasses} onSubmit={handleSubmit} id="create-automobile-form">
                        <div className="form-floating mb-3">
                            <input onChange={handleVinChange} value={vin} placeholder="Vin" required type="text" name="vin" id="vin" className="form-control" />
                            <label htmlFor="vin">Automobile Vin</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleCustomerChange} value={customer} placeholder="Customer" required type="text" name="customer" id="customer" className="form-control" />
                            <label htmlFor="customer">Customer</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleTimeChange} value={dateTime} placeholder="Date-Time" required type="datetime-local" name="date_time" id="date_time" className="form-control" />
                            <label htmlFor="date_time">Date - Time</label>
                        </div>
                        <div className="mb-3">
                            <select onChange={handleTechChange} value={technicianId} required name="model" id="model" className="form-select">
                                <option value="">Technician</option>
                                {technicians.map(tech => {
                                    return (
                                        <option key={tech.id} value={tech.id}>{tech.employee_id}</option>
                                    )
                                })};
                            </select>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleReasonChange} value={reason} placeholder="Customer" required type="text" name="reason" id="reason" className="form-control" />
                            <label htmlFor="customer">Reason for service</label>
                        </div>
                        <button className="btn btn-primary">Create</button>
                    </form>
                    <div className={messageClasses} id="success-message">
                        You have scheduled an appointment!
                    </div>
                </div>
            </div>
        </div>
    )
}

export default AppointmentForm;
