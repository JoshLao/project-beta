import { useState } from 'react';

function TechnicianForm() {
    const [firstName, setFirstName] = useState('');
    const [lastName, setLastName] = useState('');
    const [employeeId, setEmployeeId] = useState('');
    const [submittedTech, setSubmittedTech] = useState(false);

    const handleSubmit = async (e) => {
        e.preventDefault();
        const techData = {};
        techData.first_name = firstName;
        techData.last_name = lastName;
        techData.employee_id = employeeId

        const techUrl = 'http://localhost:8080/api/technicians/'
        const fetchConfig = {
            method: "POST",
            body: JSON.stringify(techData),
            headers: {
                'Content-Type': 'application/json'
            },
        };

        const techResponse = await fetch(techUrl, fetchConfig)
        if (techResponse.ok) {
            setFirstName('');
            setLastName('');
            setEmployeeId('');
        }

        setSubmittedTech(true);
    }
    const handleFirstChange = (e) => {
        const value = e.target.value;
        setFirstName(value);
    }
    const handleLastChange = (e) => {
        const value = e.target.value;
        setLastName(value);
    }
    const handleIdChange = (e) => {
        const value = e.target.value;
        setEmployeeId(value);
    }

    const formClasses = (!submittedTech) ? '' : 'd-none';
    const messageClasses = (!submittedTech) ? 'alert alert-success d-none mb-0' : 'alert alert-success mb-0';


    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Add a technician</h1>
                    <form className={formClasses} onSubmit={handleSubmit} id="create-automobile-form">
                        <div className="form-floating mb-3">
                            <input onChange={handleFirstChange} value={firstName} placeholder="First Name" required type="text" name="first_name" id="first_name" className="form-control" />
                            <label htmlFor="first_name">First Name</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleLastChange} value={lastName} placeholder="Last Name" required type="text" name="last_name" id="last_name" className="form-control" />
                            <label htmlFor="last_name">Last Name</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleIdChange} value={employeeId} placeholder="Employee ID" required type="text" name="employee_id" id="employee_id" className="form-control" />
                            <label htmlFor="employee_id">Employee ID</label>
                        </div>
                        <button className="btn btn-primary">Create</button>
                    </form>
                    <div className={messageClasses} id="success-message">
                        You have created a technician!
                    </div>
                </div>
            </div>
        </div>
    )
}

export default TechnicianForm;
