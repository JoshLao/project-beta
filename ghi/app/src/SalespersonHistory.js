import React, { useState, useEffect } from 'react';

function SalesFromSalesperson(props) {
    return (
        <table className="table table-striped">
            <thead>
                <tr>
                    <th> Salesperson </th>
                    <th> Customer </th>
                    <th> VIN </th>
                    <th> Price </th>
                </tr>
            </thead>
            <tbody>
                {

                    props.sales.filter(sale => Number(sale.salesperson.id) === Number(props.salesperson)).map(sale => {
                        return (
                            <tr key={sale.id}>
                                <td>{sale.salesperson.first_name} {sale.salesperson.last_name}</td>
                                <td> {sale.customer.first_name} {sale.customer.last_name}</td>
                                <td> {sale.automobile.vin}</td>
                                <td>{sale.price}</td>
                                <td>{sale.automobile.sold ? "Yes" : "No"}</td>
                            </tr>
                        );
                    })}
            </tbody>
        </table>
    )
}
function SalespersonHistory() {
    const [sales, setSales] = useState([])
    const [salespeople, setSalespeople] = useState([])
    const [salesperson, setSalesperson] = useState('')
    const handleSalespersonChange = (e) => {
        const value = e.target.value;
        setSalesperson(value)
    }
    useEffect(() => {
        fetchData();
    }, []);
    const fetchData = async () => {
        const salesURL = "http://localhost:8090/api/sales/"
        const salespeopleURL = "http://localhost:8090/api/salespeople/"
        const salesResponse = await fetch(salesURL);
        if (salesResponse.ok) {
            const data = await salesResponse.json()
            setSales(data.sales)
        }
        const salespeopleResponse = await fetch(salespeopleURL)
        if (salespeopleResponse.ok) {
            const data = await salespeopleResponse.json()
            setSalespeople(data.salespeople)
        }
    }
    return (
        <div>
            <div className="mb-3">
                <select onChange={handleSalespersonChange} value={salesperson} required name="salesperson" id="salesperson" className="form-select">
                    <option value="">Choose a salesperson</option>
                    {salespeople.map(salesperson => {
                        return (
                            <option key={salesperson.id} value={salesperson.id}>{salesperson.first_name} {salesperson.last_name}</option>
                        )
                    })};
                </select>
                <SalesFromSalesperson sales={sales} salesperson={salesperson} />
            </div>
        </div>
    )
}
export default SalespersonHistory;