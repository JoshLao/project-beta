import React, { useEffect, useState } from 'react';

function ManufacturerForm() {
  const [name, setName] = useState('')
  const [submitted, setSubmitted]= useState(false)
  const handleNameChange = (event) => {
    const value = event.target.value;
    setName(value);
  }


  const handleSubmit = async (event) => {
    event.preventDefault();

    const data = {
      name,
    }

    console.log(data)

    const manufacturerURL = 'http://localhost:8100/api/manufacturers/'
    const fetchConfig = {
      method: "post",
      body: JSON.stringify(data),
      headers: {
        'Content-Type': 'application/json',
      }
    }
    const manufacturerResponse = await fetch(manufacturerURL, fetchConfig)
    if (manufacturerResponse.ok) {
      const manufacturer = await manufacturerResponse.json()
      console.log(manufacturer)
      setName("")
      setSubmitted(true)

    }
  }
  const messageClasses = (!submitted) ? 'alert alert-success d-none mb-0' : 'alert alert-success mb-0';
  return (
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Create a new manufacturer</h1>
          <form onSubmit={handleSubmit} id="create-hat-form">
            <div className="form-floating mb-3">
              <input onChange={handleNameChange} value={name} placeholder="Manufacturer Name" required type="text" name="name" id="name" className="form-control" />
              <label htmlFor="name">Manufacturer Name</label>
            </div>
            <button className="btn btn-primary">Create</button>
          </form>
        </div>
        <div className={messageClasses} role="alert">
            Manufacturer created!
          </div>

      </div>
    </div>
  )

}

export default ManufacturerForm;
