import { useEffect, useState, } from 'react';
function UnsoleAutos() {
    const [automobiles, setAutomobiles] = useState([]);
    const [sales,setSales] = useState([]);

    const getData = async () => {
        const autoResponse = await fetch('http://localhost:8100/api/automobiles/');
        const salesResponse = await fetch('http://localhost:8090/api/sales/')
        if (autoResponse.ok) {
            const autoData = await autoResponse.json();
            setAutomobiles(autoData.autos)
        }
        if(salesResponse.ok){
            const salesData = await salesResponse.json();
            setSales(salesData.sales)
        }
    }
    useEffect(() => {
        getData()
    }, [])
    const checkSold = (auto) => {
        for(const sale of sales){
            if(auto.vin === sale.automobile.vin){
                return true;
            }
        }
        return false;
    }
    return (
        <table className="table table-striped">
            <thead>
                <tr>
                    <th>Vin</th>
                    <th>Color</th>
                    <th>Year</th>
                    <th>Model</th>
                    <th>Manufacturer</th>
                </tr>
            </thead>
            <tbody>
                {
                    automobiles.map(auto => {
                        if(!checkSold(auto)){
                        return (
                            <tr key={auto.href}>
                                <td>{auto.vin}</td>
                                <td>{auto.color}</td>
                                <td>{auto.year}</td>
                                <td>{auto.model.name}</td>
                                <td>{auto.model.manufacturer.name}</td>
                            </tr>
                        )
                        }
                    })}
            </tbody>
        </table>
    )
}

export default UnsoleAutos;
