from django.db import models
from django.urls import reverse
# Create your models here.
class AutomobileVO(models.Model):
    vin = models.CharField(max_length=200)
    sold = models.BooleanField()
    def __str__(self):
        return self.vin


class SalesPerson(models.Model):
    first_name = models.CharField(max_length=100)
    last_name = models.CharField(max_length=100)
    employee_id = models.IntegerField()
    def get_api_url(self):
        return reverse("api_show_salesperson", kwargs={"pk": self.pk})
    def __str__(self):
        return f"{self.first_name} {self.last_name}"



class Customer(models.Model):
    first_name = models.CharField(max_length=100)
    last_name = models.CharField(max_length=100)
    address = models.CharField(max_length=200)
    phone_number = models.CharField(max_length=200)
    def get_api_url(self):
        return reverse("api_show_customer", kwargs={"pk": self.pk})



class Sale(models.Model):
    automobile = models.ForeignKey(
        AutomobileVO,
        related_name="sales",
        on_delete=models.CASCADE,
    )
    salesperson = models.ForeignKey(
        SalesPerson,
        related_name="sales",
        on_delete=models.CASCADE,
    )
    customer = models.ForeignKey(
        Customer,
        related_name="sales",
        on_delete=models.CASCADE,
    )
    price = models.DecimalField(max_digits=10, decimal_places=2)

    def get_api_url(self):
        return reverse("api_show_sale", kwargs={"pk": self.pk})