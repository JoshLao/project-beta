from django.shortcuts import render
from .models import AutomobileVO, SalesPerson, Sale, Customer
from django.views.decorators.http import require_http_methods
from common.json import ModelEncoder
from django.http import JsonResponse
import json
# Create your views here
class AutomobileVOEncoder(ModelEncoder):
    model=AutomobileVO
    properties=[
        'vin',
        'sold',
        'id',
    ]


class SalesPersonEncoder(ModelEncoder):
    model = SalesPerson
    properties = [
        'first_name',
        'last_name',
        'employee_id',
        'id',
    ]
class CustomerEncoder(ModelEncoder):
    model = Customer
    properties = [
        'first_name',
        'last_name',
        'address',
        'phone_number',
        'id',
    ]

class SaleEncolder(ModelEncoder):
    model=Sale
    properties = [
        'automobile',
        'salesperson',
        'customer',
        'price',
        'id'
   ]
    encoders = {
        'automobile': AutomobileVOEncoder(),
        'salesperson': SalesPersonEncoder(),
        'customer': CustomerEncoder(),
    }



@require_http_methods(["GET","POST"])
def api_list_salespeople(request):
    if request.method == "GET":
        salespeople = SalesPerson.objects.all()
        return JsonResponse(
            {"salespeople": salespeople},
            encoder=SalesPersonEncoder,
        )
    else:
        content = json.loads(request.body)
        try:
            salesperson = SalesPerson.objects.create(**content)
        except:
            return JsonResponse(
                {"message": "Invalid information"},
                status=400
            )
        return JsonResponse(
            salesperson,
            encoder=SalesPersonEncoder,
            safe=False,
        )

@require_http_methods(["DELETE","GET"])
def api_show_salesperson(request,pk):
    if request.method=="DELETE":
        try:
            salesperson = SalesPerson.objects.get(id=pk)
            salesperson.delete()
            return JsonResponse({"message": "Salesperosn has been deleted"})
        except SalesPerson.DoesNotExist:
            return JsonResponse(
                {"message": "Does not exist"},
                status=400,
                )
    else:
        salesperson = SalesPerson.objects.get(id=pk)
        return JsonResponse(
            salesperson,
            encoder=SalesPersonEncoder,
            safe=False
        )


@require_http_methods(["GET","POST"])
def api_list_customers(request):
    if request.method == "GET":
        customers = Customer.objects.all()
        return JsonResponse(
            {"customers": customers},
            encoder=CustomerEncoder,
        )
    else:
        content = json.loads(request.body)
        try:
            customer = Customer.objects.create(**content)
        except:
            return JsonResponse(
                {"message": "Invalid information"},
                status=400
            )
        return JsonResponse(
            customer,
            encoder=CustomerEncoder,
            safe=False,
        )

@require_http_methods(["DELETE","GET"])
def api_show_customer(request,pk):
    if request.method=="DELETE":
        try:
            customer = Customer.objects.get(id=pk)
            customer.delete()
            return JsonResponse({"message": "Customer has been deleted"})
        except Customer.DoesNotExist:
            return JsonResponse(
                {"message": "Customer does not exist"},
                status=400,
                )
    else:
        customer = Customer.objects.get(id=pk)
        return JsonResponse(
            customer,
            encoder=CustomerEncoder,
            safe=False
        )


@require_http_methods(["GET","POST"])
def api_list_sales(request):
    if request.method == "GET":
        sales = Sale.objects.all()
        return JsonResponse(
            {"sales": sales},
            encoder=SaleEncolder,
            safe=False
        )
    else:
        content = json.loads(request.body)
        try:
            automobile_vin = content["automobile"]
            automobile = AutomobileVO.objects.get(vin=automobile_vin)
            automobile.sold = True
            automobile.save()
            print(automobile)
            content["automobile"]=automobile
        except AutomobileVO.DoesNotExist:
            return JsonResponse(
                {"message": "Automobile does not exist"},
                status=400
            )
        try:
            customer_id = content['customer_id']
            customer = Customer.objects.get(pk=customer_id)
            content['customer']=customer
        except AutomobileVO.DoesNotExist:
            return JsonResponse(
                {"message": "Customer does not exist"},
                status=400
            )
        try:
            salesperson_id=content['salesperson_id']
            salesperson = SalesPerson.objects.get(pk=salesperson_id)
            content['salesperson']=salesperson
        except SalesPerson.DoesNotExist:
            return JsonResponse(
                {"message": "Salesperson does not exist"},
                status=400
            )
        sale = Sale.objects.create(**content)
        return JsonResponse(
            sale,
            encoder=SaleEncolder,
            safe=False,
        )

@require_http_methods(["DELETE"])
def api_show_sale(request,pk):
    if request.method=="DELETE":
        try:
            sale = Sale.objects.get(id=pk)
            sale.delete()
            return JsonResponse({"message": "Sale has been deleted"})
        except Customer.DoesNotExist:
            return JsonResponse(
                {"message": "Sale does not exist"},
                status=400,
                )
    else:
        sale = Sale.objests.get(id=pk)
        return JsonResponse(
            sale,
            encoder=SaleEncolder,
            safe=False,
        )